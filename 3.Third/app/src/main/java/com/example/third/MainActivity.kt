package com.example.third

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class MainActivity: AppCompatActivity() {

    companion object {
        const val NAME_KEY = "name"
        const val PICTURE_PATH_KEY = "picture_path"
        const val REQUEST_IMAGE_CAPTURE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        action_button.setOnClickListener { onActionButtonClick() }
    }

    private var name: String? = null
    private var picturePath: String? = null

    private fun onActionButtonClick() {

        // Get written name.
        val retrievedName = name_edit.text
        if (retrievedName.isBlank()) {
            showToast(getString(R.string.missed_name_error))
            return
        }
        name = retrievedName.toString()

        // Take a picture.
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val targetComponent = takePictureIntent.resolveActivity(packageManager)
        if (targetComponent == null) {
            showToast(getString(R.string.missed_camera_app_error))
            return
        } else {
            // Prepare file.
            var pictureFile: File? = null
            try {
                val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
                val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
                pictureFile = File.createTempFile("JPEG_${timeStamp}_", ".jpg", storageDir).apply {
                    picturePath = absolutePath
                }
            } catch (e: IOException) {}
            if (pictureFile == null) {
                showToast(getString(R.string.missed_picture_file_error))
            } else {
                // Ask photo-taking app.
                val pictureUri: Uri = FileProvider.getUriForFile(this, "com.example.android.fileprovider", pictureFile)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, pictureUri)
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_IMAGE_CAPTURE) {

            // Launch next activity.
            val intent = Intent(applicationContext, ResultActivity::class.java)
            intent.putExtra(NAME_KEY, name)
            intent.putExtra(PICTURE_PATH_KEY, picturePath)
            startActivity(intent)

        }

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        if (outState != null) {
            outState.putString(NAME_KEY, name)
            outState.putString(PICTURE_PATH_KEY, picturePath)
        }

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        if (savedInstanceState != null) {
            name = savedInstanceState.getString(NAME_KEY)
            picturePath = savedInstanceState.getString(PICTURE_PATH_KEY)
        }

    }

    private fun showToast(text: String) {
        Toast.makeText(applicationContext, text, Toast.LENGTH_LONG).show()
    }

}
