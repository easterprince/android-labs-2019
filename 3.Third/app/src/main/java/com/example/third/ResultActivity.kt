package com.example.third

import android.graphics.BitmapFactory
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import kotlinx.android.synthetic.main.activity_result.*
import android.R.attr.bitmap
import android.media.ExifInterface
import android.graphics.Bitmap
import android.graphics.BitmapRegionDecoder
import android.graphics.Matrix


class ResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        // Get and show name.
        val name = intent.getStringExtra(MainActivity.NAME_KEY)
        result_text_view.text = getString(R.string.result_template).format(name)

        // Get and show picture.
        val ei = ExifInterface(intent.getStringExtra(MainActivity.PICTURE_PATH_KEY))
        val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
        var picture = BitmapFactory.decodeFile(intent.getStringExtra(MainActivity.PICTURE_PATH_KEY))!!
        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> picture = rotateImage(picture, 90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> picture = rotateImage(picture, 180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> picture = rotateImage(picture, 270f)
        }
        result_image_view.setImageBitmap(picture)

    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
    }

}
