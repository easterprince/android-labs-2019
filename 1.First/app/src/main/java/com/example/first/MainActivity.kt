package com.example.first

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private fun scream(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        scream("onCreate()")
    }

    override fun onStart() {
        super.onStart()
        scream("onStart()")
    }

    override fun onResume() {
        super.onResume()
        scream("onResume()")
    }

    override fun onPause() {
        super.onPause()
        scream("onPause()")
    }

    override fun onStop() {
        super.onPause()
        scream("onStop()")
    }

    override fun onDestroy() {
        super.onDestroy()
        scream("onDestroy()")
    }

}
