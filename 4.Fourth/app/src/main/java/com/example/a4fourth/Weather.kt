package com.example.a4fourth

import java.lang.StringBuilder

class Weather {

    var date: String? = null
    var tod: String? = null
    var cloud: String? = null
    var temp: String? = null
    var pressure: String? = null
    var humidity: String? = null
    var wind: String? = null

    constructor(
        date: String,
        tod: String,
        cloud: String,
        temp: String,
        pressure: String,
        humidity: String,
        wind: String
    ) {
        this.date = date
        this.tod = tod
        this.cloud = cloud
        this.temp = temp
        this.pressure = pressure
        this.humidity = humidity
        this.wind = wind
    }

    fun determineImage(): Int {
        if (cloud == null) {
            return R.drawable.rd_derp
        }
        val text: String = cloud!!.toLowerCase()
        return when {
            text.contains("гроза") -> R.drawable.rd_storm
            text.contains("снег") -> R.drawable.rd_snow
            text.contains("дождь") -> R.drawable.rd_rain
            text.contains("пасмурно") -> R.drawable.rd_greyness
            text.contains("облачно") -> R.drawable.rd_cloud
            text.contains("ясно") -> R.drawable.rd_sun
            else -> R.drawable.rd_derp
        }
    }

    fun formFirstDescription(): String {
        val result = StringBuilder()
        date?.let { result.append(it).append(":\n") }
        cloud?.let { result.append(it).append(", ") }
        temp?.let { result.append(it).append("°C") }
        return result.toString()
    }

    fun formSecondDescription(): String {
        val result = StringBuilder()
        date?.let { result.append(it).append(", ") }
        tod?.let { result.append(
            when (tod) {
                "0" -> "ночь"
                "1" -> "утро"
                "2" -> "день"
                "3" -> "вечер"
                else -> "неизвестно"
            }
        ).append(":\n") }
        cloud?.let { result.append(it).append(", ") }
        temp?.let { result.append(it).append("°C, ") }
        pressure?.let { result.append(it).append(" mb, ") }
        humidity?.let { result.append(it).append("%, ") }
        wind?.let { result.append(it) }
        return result.toString()
    }

}