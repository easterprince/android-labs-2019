package com.example.a4fourth

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.second_weather_fragment.*
import kotlinx.android.synthetic.main.second_weather_fragment.view.*

class SecondWeatherFragment : Fragment() {

    companion object {
        const val WEATHER_DESCRIPTION_KEY_PREFIX = "weather_description_"
        const val WEATHER_IMAGE_KEY_PREFIX = "weather_image_"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.second_weather_fragment, container, false)

        for (tod in 0..3) {
            val text = arguments?.getString(WEATHER_DESCRIPTION_KEY_PREFIX + tod)
            if (text != null) {
                val textView = when (tod) {
                    0 -> view.weather_text_view_0
                    1 -> view.weather_text_view_1
                    2 -> view.weather_text_view_2
                    3 -> view.weather_text_view_3
                    else -> null
                }
                textView?.text = text
            }
            val image = arguments?.getInt(WEATHER_IMAGE_KEY_PREFIX + tod)
            if (image != null && image != 0) {
                val imageView = when (tod) {
                    0 -> view.weather_image_view_0
                    1 -> view.weather_image_view_1
                    2 -> view.weather_image_view_2
                    3 -> view.weather_image_view_3
                    else -> null
                }
                imageView?.setImageResource(image)
            }
        }

        return view
    }

}