package com.example.a4fourth

import android.content.ContentValues
import android.content.Context
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class WeatherDatabase(val context: Context) {

    companion object {

        const val CITY_ID = 24

        var instance: WeatherDatabase? = null
            private set

        fun setApplicationContext(applicationContext: Context) {
            synchronized(WeatherDatabase::class.java) {
                if (instance == null) {
                    instance = WeatherDatabase(applicationContext)
                }
            }
        }

    }

    private val sqlHelper = WeatherSqlHelper(context)
    private val database = sqlHelper.writableDatabase

    fun collectWeather(): ArrayList<Weather> {
        val result = ArrayList<Weather>()

        val cursor = database.query(WeatherSqlHelper.TABLE_NAME, arrayOf("*"), null, null, null, null, null)
        val dateIndex = cursor.getColumnIndex(WeatherSqlHelper.COLUMN_DATE)
        val todIndex = cursor.getColumnIndex(WeatherSqlHelper.COLUMN_TOD)
        val cloudIndex = cursor.getColumnIndex(WeatherSqlHelper.COLUMN_CLOUD)
        val tempIndex = cursor.getColumnIndex(WeatherSqlHelper.COLUMN_TEMP)
        val pressureIndex = cursor.getColumnIndex(WeatherSqlHelper.COLUMN_PRESSURE)
        val humidityIndex = cursor.getColumnIndex(WeatherSqlHelper.COLUMN_HUMIDITY)
        val windIndex = cursor.getColumnIndex(WeatherSqlHelper.COLUMN_WIND)
        while (cursor.moveToNext()) {
            result.add(Weather(
                cursor.getString(dateIndex),
                cursor.getString(todIndex),
                cursor.getString(cloudIndex),
                cursor.getString(tempIndex),
                cursor.getString(pressureIndex),
                cursor.getString(humidityIndex),
                cursor.getString(windIndex)
            ))
        }
        cursor.close()

        return result
    }

    fun placeWeather(successAction: () -> Unit, failureAction: () -> Unit, forceUpdate: Boolean = false) {

        if (!forceUpdate) {
            var place: Boolean = false
            val cursor = database.query(WeatherSqlHelper.TABLE_NAME, arrayOf("*"), null, null, null, null, null)
            if (cursor.count != 0) {
                successAction()
                cursor.close()
                return
            }
            cursor.close()
        }

        val retrofit = Retrofit.Builder()
            .baseUrl("http://icomms.ru/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service = retrofit.create<Magic>(Magic::class.java)
        val call = service.getWeatherList(CITY_ID)
        call.enqueue(object : Callback<List<Weather>> {
            override fun onResponse(call: Call<List<Weather>>, response: Response<List<Weather>>) {
                val data = (response.body() as? ArrayList<Weather>)!!
                sqlHelper.clearTable()
                for (weather in data) {
                    val contentValues = ContentValues()
                    contentValues.put(WeatherSqlHelper.COLUMN_DATE, weather.date)
                    contentValues.put(WeatherSqlHelper.COLUMN_TOD, weather.tod)
                    contentValues.put(WeatherSqlHelper.COLUMN_CLOUD, weather.cloud)
                    contentValues.put(WeatherSqlHelper.COLUMN_TEMP, weather.temp)
                    contentValues.put(WeatherSqlHelper.COLUMN_PRESSURE, weather.pressure)
                    contentValues.put(WeatherSqlHelper.COLUMN_HUMIDITY, weather.humidity)
                    contentValues.put(WeatherSqlHelper.COLUMN_WIND, weather.wind)
                    database.insert(WeatherSqlHelper.TABLE_NAME, null, contentValues)
                }
                successAction()
            }
            override fun onFailure(call: Call<List<Weather>>, t: Throwable) {
                failureAction()
            }
        })

    }

}