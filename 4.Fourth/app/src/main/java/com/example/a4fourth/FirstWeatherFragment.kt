package com.example.a4fourth

import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Color
import android.os.Bundle
import android.provider.CalendarContract
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.first_weather_fragment.*
import kotlinx.android.synthetic.main.first_weather_fragment.view.*




class FirstWeatherFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {

    companion object {
        private const val SHOW_TOD = "2"
    }

    private var allWeather: ArrayList<Weather>? = null
    private var dailyWeather: ArrayList<Weather>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        WeatherDatabase.setApplicationContext(activity!!.applicationContext)
        val view = inflater.inflate(R.layout.first_weather_fragment, container, false)
        knock(view, false)
        view.swipe_container.setOnRefreshListener(this)
        view.swipe_container.setColorSchemeColors(
            Color.BLUE,
            Color.RED,
            Color.CYAN,
            Color.MAGENTA
        );
        return view
    }

    private fun showSecondWeather(source: List<Weather>) {
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {

            val intent = Intent(context, SecondActivity::class.java)
            for (weather in source) {
                var key: String
                key = SecondActivity.WEATHER_DESCRIPTION_KEY_PREFIX + weather.tod
                intent.putExtra(key, weather.formSecondDescription())
                key = SecondActivity.WEATHER_IMAGE_KEY_PREFIX + weather.tod
                intent.putExtra(key, weather.determineImage())
            }

            startActivity(intent)

        } else {

            val secondFragment = SecondWeatherFragment()
            val bundle = Bundle()
            for (weather in source) {
                var key: String
                key = SecondWeatherFragment.WEATHER_DESCRIPTION_KEY_PREFIX + weather.tod
                bundle.putString(key, weather.formSecondDescription())
                key = SecondWeatherFragment.WEATHER_IMAGE_KEY_PREFIX + weather.tod
                bundle.putInt(key, weather.determineImage())
            }
            secondFragment.arguments = bundle

            fragmentManager!!
                .beginTransaction()
                .replace(R.id.second_weather_frame, secondFragment)
                .commit()

        }
    }

    override fun onRefresh() {
        knock(view!!, true)
    }

    private fun knock(view: View, forceUpdate: Boolean) {
        WeatherDatabase.instance!!.placeWeather({
            allWeather = WeatherDatabase.instance!!.collectWeather()
            dailyWeather = ArrayList(allWeather!!.filter { weather -> weather.tod == SHOW_TOD })
            val dailyWeatherAdapter = DailyWeatherAdapter(dailyWeather!!)
            dailyWeatherAdapter.onClickListener = View.OnClickListener { view ->
                val position = (view.tag as RecyclerView.ViewHolder).adapterPosition
                val chosen = allWeather!!.filter { weather -> weather.date == dailyWeather!![position].date }
                showSecondWeather(chosen)
            }
            view.daily_weather_recycler_view.layoutManager = LinearLayoutManager(activity!!.applicationContext)
            view.daily_weather_recycler_view.adapter = dailyWeatherAdapter
            swipe_container?.setRefreshing(false);
        }, {
            Toast.makeText(activity!!.application.applicationContext, "Can't get weather data.", Toast.LENGTH_LONG).show()
            swipe_container?.setRefreshing(false);
        }, forceUpdate)
    }

}