package com.example.a4fourth

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.first_weather_item.view.*

class DailyWeatherAdapter(public var items: ArrayList<Weather>) : RecyclerView.Adapter<DailyWeatherAdapter.ViewHolder>() {
    var onClickListener: View.OnClickListener? = null

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.first_weather_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.weatherTextView.text = items[position].formFirstDescription()
        holder.weatherImageView.setImageResource(items[position].determineImage())
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val weatherTextView: TextView = view.weather_text_view
        val weatherImageView: ImageView = view.weather_image_view

        init {
            view.tag = this
            view.setOnClickListener(onClickListener)
        }

    }

}