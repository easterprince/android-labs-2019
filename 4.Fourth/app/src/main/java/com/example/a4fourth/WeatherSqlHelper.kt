package com.example.a4fourth

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class WeatherSqlHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {

        const val DATABASE_NAME = "weather.db"
        const val DATABASE_VERSION = 1

        const val TABLE_NAME = "weather"
        const val COLUMN_DATE = "date"
        const val COLUMN_TOD = "tod"
        const val COLUMN_CLOUD = "cloud"
        const val COLUMN_TEMP = "temp"
        const val COLUMN_PRESSURE = "pressure"
        const val COLUMN_HUMIDITY = "humidity"
        const val COLUMN_WIND = "wind"

    }

    override fun onCreate(database: SQLiteDatabase) {
        database.execSQL(
            "CREATE TABLE " + TABLE_NAME +
                " (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_DATE + " VARCHAR(64), " +
                COLUMN_TOD + " VARCHAR(64), " +
                COLUMN_CLOUD + " VARCHAR(255), " +
                COLUMN_TEMP + " VARCHAR(64), " +
                COLUMN_PRESSURE + " VARCHAR(64), " +
                COLUMN_HUMIDITY + " VARCHAR(64), " +
                COLUMN_WIND + " VARCHAR(64))"
        )
    }

    override fun onUpgrade(database: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {}

    fun clearTable() {
        writableDatabase.execSQL("DELETE FROM " + TABLE_NAME);
    }

}