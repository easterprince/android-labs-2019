package com.example.a4fourth

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    companion object {
        const val WEATHER_DESCRIPTION_KEY_PREFIX = SecondWeatherFragment.WEATHER_DESCRIPTION_KEY_PREFIX
        const val WEATHER_IMAGE_KEY_PREFIX = SecondWeatherFragment.WEATHER_IMAGE_KEY_PREFIX
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val secondFragment = SecondWeatherFragment()
        val bundle = Bundle()
        for (tod in 0..3) {
            var key: String
            key = WEATHER_DESCRIPTION_KEY_PREFIX + tod
            intent.getStringExtra(key)?.let { bundle.putString(key, it) }
            key = WEATHER_IMAGE_KEY_PREFIX + tod
            intent.getIntExtra(key, 0).let { bundle.putInt(key, it) }
        }
        secondFragment.arguments = bundle

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.second_weather_frame, secondFragment)
            .commit()

    }

}
