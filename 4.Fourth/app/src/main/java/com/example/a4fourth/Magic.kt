package com.example.a4fourth

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface Magic {
    @GET("inf/meteo.php")
    fun getWeatherList(@Query("tid") tid: Int): Call<List<Weather>>
}