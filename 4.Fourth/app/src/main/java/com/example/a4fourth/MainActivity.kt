package com.example.a4fourth

import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val firstFragment = FirstWeatherFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.first_weather_frame, firstFragment)
            .addToBackStack(null)
            .commit()

    }

}
