package com.example.second

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import java.math.BigInteger

class MainActivity : AppCompatActivity() {

    private enum class Operation {
        ADDITION,
        SUBTRACTION,
        MULTIPLICATION,
        DIVISION
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addition_button.setOnClickListener { performOperation(Operation.ADDITION) }
        subtraction_button.setOnClickListener { performOperation(Operation.SUBTRACTION) }
        multiplication_button.setOnClickListener { performOperation(Operation.MULTIPLICATION) }
        division_button.setOnClickListener { performOperation(Operation.DIVISION) }
    }

    private fun performOperation(operation: Operation) {

        var forceExit = false

        // Get input.
        val first = first_number_edit.text.toString().toBigIntegerOrNull()
        if (first == null) {
            first_number_edit.error = getString(R.string.wrong_integer_error)
        } else {
            first_number_edit.error = null
        }
        val second = second_number_edit.text.toString().toBigIntegerOrNull()
        if (second == null) {
            second_number_edit.error = getString(R.string.wrong_integer_error)
        } else if (operation == Operation.DIVISION && second == BigInteger.ZERO) {
            second_number_edit.error = getString(R.string.zero_divisor_error)
            forceExit = true
        } else {
            second_number_edit.error = null
        }
        if (forceExit || first == null || second == null) {
            Snackbar.make(main_layout, R.string.rewrite_demand, Snackbar.LENGTH_LONG).show()
            return
        }

        // Perform operation.
        val answer = when (operation) {
            Operation.ADDITION -> first + second
            Operation.SUBTRACTION -> first - second
            Operation.MULTIPLICATION -> first * second
            Operation.DIVISION -> first / second
        }
        answer_text_view.text = "$answer"

    }

}
